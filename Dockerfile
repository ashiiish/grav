FROM phusion/baseimage

MAINTAINER Ashish Shah <ashneo76@gmail.com>

# CMD ["/sbin/my_init"]

ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive

# Install core packages
RUN apt-get update -q
RUN apt-get upgrade -y -q
# Install NGINX
RUN apt-get install -y nginx ca-certificates git-core wget zsh
# Install PHP 7.0
RUN apt-get install -y -q php7.0 php7.0-curl php7.0-gd php7.0-fpm php7.0-cli php7.0-opcache php7.0-mbstring php7.0-xml php7.0-zip php-apcu
RUN apt-get clean -q && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Grav
COPY scripts/install_grav.sh /scripts/install_grav.sh
COPY scripts/fix_permissions.sh /scripts/fix_permissions.sh
COPY scripts/backup.sh /scripts/backup.sh
COPY scripts/restore.sh /scripts/restore.sh
COPY scripts/setup_user.sh /scripts/setup_user.sh

WORKDIR /
ENV TGT_DIR /usr/share/nginx/html
RUN \
    echo "Downloading grav to $TGT_DIR" && \
    rm -rf $TGT_DIR && \
    git clone https://github.com/getgrav/grav.git $TGT_DIR

WORKDIR $TGT_DIR
RUN /scripts/install_grav.sh
RUN echo "Finished Grav install"

# Install Nginx
ENV NGINX_DIR /etc/nginx
RUN sed -ie "s|# gzip_types|  gzip_types|" /etc/nginx/nginx.conf

ENV DEFAULT_SITE /etc/nginx/sites-available/default
RUN cp /usr/share/nginx/html/webserver-configs/nginx.conf $DEFAULT_SITE
RUN sed -ie "s|root.*$|root /usr/share/nginx/html;|" $DEFAULT_SITE
# RUN sed -ie "s|fastcgi_param SCRIPT_FILENAME|# fastcgi_param SCRIPT_FILENAME|" $DEFAULT_SITE
RUN sed -ie "s|fastcgi_param SCRIPT_FILENAME.*$|fastcgi_param SCRIPT_FILENAME \$document_root/\$fastcgi_script_name;|" $DEFAULT_SITE
RUN sed -ie "s|fastcgi_pass.*$|fastcgi_pass unix:/run/php/php7.0-fpm.sock;|" $DEFAULT_SITE
RUN mkdir -p /run/php

# Install the run files
COPY etc/service/nginx/run /etc/service/nginx/run
COPY etc/service/php7.0-fpm/run /etc/service/php7.0-fpm/run

# Add onbuild and later instructions
ONBUILD ARG SERVER_NAME
ONBUILD ENV SERVER_NAME ${SERVER_NAME:-localhost}
ONBUILD RUN echo "Setting Server Name: $SERVER_NAME"
# ONBUILD RUN sed -ie "s|server_name.*$|server_name $SERVER_NAME;|" $DEFAULT_SITE
# ONBUILD RUN su -s /usr/bin/zsh -c /scripts/setup_user.sh www-data

EXPOSE 80
WORKDIR $TGT_DIR
