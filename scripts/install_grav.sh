#!/bin/bash
set -e

echo "Installing dependencies" && bin/composer.phar self-update && bin/grav install

/scripts/fix_permissions.sh /usr/share/nginx/html

echo "Finshed Grav install."
