#!/bin/bash
set -e

HTML_DIR=${1:-/usr/share/nginx/html}
USER=${2:-www-data}
GROUP=${3:-$USER}

echo "Fixing permissions on: ${HTML_DIR} [${USER}:${GROUP}]..." && \
    chown -R ${USER}:${GROUP} ${HTML_DIR} && \
    find ${HTML_DIR} -type f | xargs chmod 664 && \
    find ${HTML_DIR} -type d | xargs chmod 775 && \
    find ${HTML_DIR} -type d | xargs chmod +s && \
    umask 0002 && \
    chmod a+x ${HTML_DIR}/bin/*

mkdir -p /var/www
chown -R www-data:www-data /var/www
