.PHONY: build

IMAGE_NAME = "ashneo76/grav"
DOCKER = docker

build:
	$(DOCKER) build -t $(IMAGE_NAME) .

tests:
	$(DOCKER) info

deploy:
	$(DOCKER) login --username=$(DOCKER_USERNAME) --password=$(DOCKER_PASSWORD)
	$(DOCKER) push $(IMAGE_NAME)

build_gl:
	$(DOCKER) build -t registry.gitlab.com/$(IMAGE_NAME) .

deploy_gl:
	$(DOCKER) login registry.gitlab.com
	$(DOCKER) push registry.gitlab.com/$(IMAGE_NAME)
